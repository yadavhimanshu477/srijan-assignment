import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from '../../src/app.controller';
import { AppService } from '../../src/app.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getHello()).toBe('Hello World!');
    });

    it('should return array of FizzBuzzPattern data', () => {
      const expectedData = [ "1", "2", "Fizz", "4", "Buzz" ]
      const bodyData = { 'count': 5 }
      expect(appController.getPatternData(bodyData).data).toEqual(expectedData);
    });
  });
});
