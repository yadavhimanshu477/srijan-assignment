import { BadRequestException } from '@nestjs/common'
import { plainToClass } from 'class-transformer'
import { validate } from 'class-validator'
import { PipeTransform, Injectable, ArgumentMetadata, HttpStatus } from '@nestjs/common'
import { ResponseMessage } from '../interceptor/messages/responseMessage'

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
    private static thisObject: ValidationPipe

    static instance(): ValidationPipe {
        if (this.thisObject) {
            return this.thisObject
        }
        this.thisObject = new ValidationPipe()
        return this.thisObject
    }
    async transform(value, metadata: ArgumentMetadata) {
        const { metatype } = metadata
        if (!metatype || !this.toValidate(metatype)) {
            return value
        }

        const object = plainToClass(metatype, value)
        const errors = await validate(object)
        console.log(errors)
        // TODO: Log the errors
        if (errors.length > 0) {
            let errorMessage = ''
            for (let error of errors) {
                errorMessage += ' ' + error.property + '->'
                if (error.constraints) {
                    let keys = Object.keys(error.constraints)
                    for (let key of keys) {
                        errorMessage += error.constraints[key] + ','
                    }
                }
                if (error.children && error.children.length > 0) {
                    for (let childError of error.children) {
                        errorMessage += this.getConstraintsFailedMessage(childError)
                    }
                    // errorMessage = this.getConstraintsFailedMessage(error, errorMessage);
                }
            }
            throw new BadRequestException({ message: ResponseMessage.R0007, status: HttpStatus.BAD_REQUEST, details: errorMessage })
        }
        return value
    }

    private getConstraintsFailedMessage(error): string {
        if (error.constraints) {
            let errorMessage = error.property + '->'
            let keys = Object.keys(error.constraints)
            for (let key of keys) {
                errorMessage += error.constraints[key] + ','
            }
            return errorMessage
        }

        if (error.children && error.children.length > 0) {
            let errorMessage = ''
            for (let childError of error.children) {
                errorMessage += this.getConstraintsFailedMessage(childError)
            }
            return errorMessage
        }
    }

    private toValidate(metatype): boolean {
        const types = [String, Boolean, Number, Array, Object]
        return !types.find(type => metatype === type)
    }
}
