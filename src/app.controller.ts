import { Controller, Get, Post, Body, HttpStatus, UsePipes } from '@nestjs/common';
import { AppService } from './app.service';
import { AppDto } from './dto/app.dto';
import { ResponseObject } from './interceptor/response.type';
import { ResponseMessage } from './interceptor/messages/responseMessage';
import { ValidationPipe } from './pipes/validation.pipe';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): String {
    return this.appService.getHello();
  }

  @Get('getFizzBuzzData')
  getData(): ResponseObject {
    return {  message: ResponseMessage.R0008 ,status: HttpStatus.OK, data: [] };
  }

  @UsePipes(ValidationPipe.instance())
  @Post('getFizzBuzzData')
  getPatternData(@Body() bodyData: AppDto): ResponseObject {
    const count = bodyData.count
    return {  message: ResponseMessage.R0000 ,status: HttpStatus.OK, data: this.appService.getFizzBuzzData(count) };
  }
}
