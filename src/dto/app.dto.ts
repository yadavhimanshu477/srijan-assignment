import { IsNotEmpty } from "class-validator";

export class AppDto {
    @IsNotEmpty()
    readonly count: Number;
}