import { ExecutionContext, Injectable, Logger, NestInterceptor } from '@nestjs/common'
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs'

@Injectable()
export class ResponseInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, call$: Observable<any>): Observable<any> {
        const response = context.switchToHttp().getResponse()

        return call$.pipe(
            map(res => {
                if (!res || !res.message || !res.status) {
                    Logger.log('Bad Gateway')
                    response.status(502).json({ message: 'Bad Gateway' })
                }

                const resData = res.data || undefined
                response.status(res.status).json({ message: res.message, data: resData })
            }),
        )
    }
}
