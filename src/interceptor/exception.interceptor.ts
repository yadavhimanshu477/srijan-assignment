import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus, Logger } from '@nestjs/common'
import { ErrorMessage } from './messages/errorMessage'

@Catch()
export class ExceptionInterceptor implements ExceptionFilter {
    catch(error: Error, host: ArgumentsHost) {
        Logger.log(error)
        const response = host.switchToHttp().getResponse()
        const status = error instanceof HttpException ? error.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR
        let errorObject = {}
        errorObject['message'] = !this.isDeepNull(error, 'message') ? error['message'] : ErrorMessage.E0001
        if (!this.isDeepNull(error, 'stack')) {
            errorObject['details'] = error['stack']
        }
        return response.status(status).json(errorObject)
    }

    isDeepNull(obj, ...keys) {
        return keys.length === 0 ? this.isNullOrUndefined(obj) : (() => this.isNullOrUndefined(obj) ? true : this.isDeepNull(obj[keys[0]], ...keys.splice(1, keys.length - 1)))()
    }

    isNullOrUndefined(obj) {
        return undefined === obj || obj === null
    }
}
