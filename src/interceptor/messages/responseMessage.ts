export enum ResponseMessage {
  R0000 = 'Respond successfully',
  R0001 = 'Not Found',
  R0007 = 'Required Field Not Provided.',
  R0008 = 'Use POST method in staed of GET and pass count in request body'
}
