import { ErrorMessage } from './messages/errorMessage'
import { HttpStatus } from '@nestjs/common'
import { ResponseMessage } from './messages/responseMessage'

export interface ResponseObject {
    status: HttpStatus
    message: ErrorMessage | ResponseMessage
    data?: any
}
