import { HttpException } from "@nestjs/common";

export class ApplicationException extends HttpException{
    
}