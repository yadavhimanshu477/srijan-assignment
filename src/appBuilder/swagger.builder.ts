import { Builder } from './builder.interface'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'
import { BuilderRegistry } from './builder.registry'

export class SwaggerBuilder implements Builder {

    private static thisObject: SwaggerBuilder
    private constructor() { }

    static instance(builderRegistry: BuilderRegistry): Builder {
        if (this.thisObject) {
            return this.thisObject
        }
        this.thisObject = new SwaggerBuilder()
        builderRegistry.registerBuilder(this.thisObject)
        return this.thisObject
    }

    build(app: any) {
        const options = new DocumentBuilder()
            .setTitle('NestJs Example')
            .setDescription('NestJs Api Description')
            .setVersion('1.0')
            .addTag('NestJsProject')
            .addBearerAuth()
            .build()
        const document = SwaggerModule.createDocument(app, options)
        // SwaggerModule.setup('api', app, document)
    }
}
