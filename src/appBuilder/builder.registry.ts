import {Builder} from './builder.interface'
import {BuildersLoader} from './builder.loader'

export class BuilderRegistry {

    private builders: Builder[] = []
    private static thisObject: BuilderRegistry
    // tslint:disable-next-line:no-empty
    private constructor() {
        BuildersLoader.loadAllBuilders(this)
    }

    static instance(): BuilderRegistry {
        if (this.thisObject) {
            return this.thisObject
        }
        this.thisObject = new BuilderRegistry()
        return this.thisObject
    }

    registerBuilder(builder: Builder): void {
        this.builders.push(builder)
    }

    getAllBuilders(): Builder[] {
        return this.builders
    }

    buildAll(app: any) {
        this.getAllBuilders().forEach ((builder) => {
            builder.build(app)
        })
    }
}
