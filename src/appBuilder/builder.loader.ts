import { BuilderRegistry } from './builder.registry'
import { SwaggerBuilder } from './swagger.builder'
import { InterceptorBuilder } from './interceptor.builder'

export class BuildersLoader {
    // tslint:disable-next-line:no-empty
    private constructor() {}

    static loadAllBuilders(builderRegistry: BuilderRegistry) {
        SwaggerBuilder.instance(builderRegistry)
        InterceptorBuilder.instance(builderRegistry)
    }
}
