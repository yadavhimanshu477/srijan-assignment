import { Builder } from './builder.interface'
import { ResponseInterceptor } from '../interceptor/response.interceptor'
import { ExceptionInterceptor } from '../interceptor/exception.interceptor'
import { BuilderRegistry } from './builder.registry'
import { json } from 'body-parser'

export class InterceptorBuilder implements Builder {

    private static thisObject: InterceptorBuilder
    private constructor() { }

    static instance(builderRegistry: BuilderRegistry): Builder {
        if (this.thisObject) {
            return this.thisObject
        }
        this.thisObject = new InterceptorBuilder()
        builderRegistry.registerBuilder(this.thisObject)
        return this.thisObject
    }

    build(app: any) {
        app.useGlobalInterceptors(new ResponseInterceptor())
        app.useGlobalFilters(new ExceptionInterceptor())
        app.use(json({ limit: '50mb'}))
    }
}
