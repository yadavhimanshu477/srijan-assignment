import { INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { BuilderRegistry } from './appBuilder/builder.registry';

async function bootstrap() {
  const app: INestApplication = await NestFactory.create(AppModule);
  const builderRegistry: BuilderRegistry = BuilderRegistry.instance();

  builderRegistry.buildAll(app);
  await app.listen(3010);
}
bootstrap();
