import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  getFizzBuzzData(count): String[] {
    const arr: String[] = [];
    const n = count;
    // loop for n(count) times
    for (let i = 1; i <= n; i++) {
      if (i % 15 == 0) {
        arr.push("FizzBuzz");
        console.log("FizzBuzz");
      }
      // number divisible by 5, print 'Buzz'
      // in place of the number
      else if (i % 5 == 0) {
        arr.push("Buzz");
        console.log("Buzz");
      }
      // number divisible by 3, print 'Fizz'
      // in place of the number
      else if (i % 3 == 0) {
        arr.push("Fizz");
        console.log("Fizz");
      }
      // number divisible by 15(divisible by
      // both 3 & 5), print 'FizzBuzz' in
      // place of the number
      else {// print the numbers
        arr.push(''+i);
        console.log(i);
      }
    }
    return arr;
  }
}
