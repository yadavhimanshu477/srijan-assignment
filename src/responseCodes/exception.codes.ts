export const ExceptionCodes = {
    500 : 'INTERNAL_SERVER_ERROR',
    400 : 'Bad Request'
};
