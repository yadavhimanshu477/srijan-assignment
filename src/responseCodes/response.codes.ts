export const ResponseCodes = {
    INSERTED_SUCCESSFULLY : 'Inserted Successfully',
    FETCHED_SUCCESSFULLY : 'Entries data fetched successfull',
    UPDATED_SUCCESSFULLY : 'Entries data Updated successfully'
};
